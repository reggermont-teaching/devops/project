# Final Project

## Required features

For the average note for a team of 2:

- Pipelines triggered at change: Minimum jobs expected:
  - Test (for Laravel project: `php artisan test`)
  - Deployment (only master and deploying on all team members' Heroku / VPS)
- Secured sensible variables
- Specific [README.md](#readme_expected) (see below)


## Facultative features

At least one required for a team of 3+:

- Dockerised CD
- Additional features and jobs (ex: lint) with parallelized jobs / job dependencies
- Production CI/CD (other specific jobs + deployment on one of the team members' Heroku / VPS)
- Single job for dependencies installation
- ...

## Readme expected

```md
# Any title

## Description

One-line description of the project

## Teams members

- Member n°1
- Member n°2
- Member n°3

## What has been done

- Pipelines triggered at change: working test and deployment jobs
- Facultative feature n°1
- Facultative feature n°2
- Facultative feature n°3
- ...

## Deployment URLs

- http://heroku-vps.n°1
- http://heroku-vps.n°2
- http://heroku-vps.n°3
```

## Oral presentation

- Date: 2020-10-27
- Schedules and duration to come
- Presentation proposal:
  - What has been done (describe `gitlab-ci.yml`)
  - Who worked on what
  - Difficulties encountered
  - Specific part you're proud of
